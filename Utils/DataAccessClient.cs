﻿using Assignment2_AppendixB.Repositories.Customers;
using Assignment2_AppendixB.Repositories.Country;
using Assignment2_AppendixB.Repositories.Spender;
using Assignment2_AppendixB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2_AppendixB.Repositories.Genre;

namespace Assignment2_AppendixB.Utils
{
    internal class DataAccessClient
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly ICustomerCountryRepository _customerCountry;
        private readonly ICustomerSpenderRepository _spenderRepository;
        private readonly ICustomerGenreRepository _customerGenreRepository;

        public DataAccessClient(ICustomerRepository customerRepository, ICustomerCountryRepository customerCountry, ICustomerSpenderRepository customerSpenderRepository, ICustomerGenreRepository customerGenreRepository)
        {
            _customerRepository = customerRepository;
            _customerCountry = customerCountry;
            _spenderRepository = customerSpenderRepository;
            _customerGenreRepository = customerGenreRepository;
        }
        /// <summary>
        /// Method to add functionality to console. 
        /// </summary>
        public bool ExecuteCustomerDataAccess()
        {
            Console.WriteLine("\nWhat task do you want to have a look at?");
            Console.WriteLine("1. Read all customers in database.");
            Console.WriteLine("2. Read a specific customer (Choose your own ID)");
            Console.WriteLine("3. Read a specific customer by name (Choose your own name)");
            Console.WriteLine("4. Return a page of customers.");
            Console.WriteLine("5. Add a new customer to the database.");
            Console.WriteLine("6. Update an existing user. ");
            Console.WriteLine("7. Read number of customers in each country.");
            Console.WriteLine("8. Read the highest spenders.");
            Console.WriteLine("9. A customers most popular genre (Choose your own ID)");
            Console.WriteLine("0. Quit playing");
            Console.WriteLine("\n Select an option: ");
            switch( Console.ReadLine())
            {
                case "0":
                    return false;
                case "1":
                    Console.Clear();
                    Console.WriteLine("---------------------- Part 1: -----------------------------");
                    _customerRepository.GetAll().ForEach(c => Console.WriteLine(c));
                    return true;
                case "2":
                    Console.Clear();
                    Console.WriteLine("---------------------- Part 2: -----------------------------");
                    Console.WriteLine("What ID do you want to check?");
                    int chosenId = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine(_customerRepository.GetByID(chosenId));
                    return true;
                case "3":
                    Console.Clear();
                    Console.WriteLine("---------------------- Part 3: -----------------------------");
                    Console.WriteLine("What Name do you want to check? (Luis exists in DB)");
                    string? chosenName = Console.ReadLine();
                    Console.WriteLine(_customerRepository.GetByName(chosenName ?? "Luis"));
                    return true;
                case "4":
                    Console.Clear();
                    Console.WriteLine("---------------------- Part 4: -----------------------------");
                    _customerRepository.GetAPage().ForEach(c => Console.WriteLine(c));
                    return true;
                case "5":
                    Console.Clear();
                    Console.WriteLine("---------------------- Part 5: -----------------------------");
                    _customerRepository.Add(new Customer(0, "Tore", "Tang", "Norway", "0572", "45667167", "tore.tang@gmail.com"));
                    Console.WriteLine("Customer added!");
                    return true;
                case "6":
                    Console.Clear();
                    Console.WriteLine("----------------------- Part 6: ----------------------------");
                    Console.WriteLine("Customer Tore changed!");
                    _customerRepository.Update(new Customer(60, "Tore", "Tang", "Norway", "4200", "42066642", "Tore_420@Tang666.com"));
                    return true;
                case "7":
                    Console.Clear();
                    Console.WriteLine("----------------------- Part 7: ----------------------------");
                    _customerCountry.GetAll().ForEach(c => Console.WriteLine(c));
                    return true;
                case "8":
                    Console.Clear();
                    Console.WriteLine("----------------------- Part 8: ----------------------------");
                    _spenderRepository.GetAll().ForEach(c => Console.WriteLine(c));
                    return true;
                case "9":
                    Console.Clear();
                    Console.WriteLine("----------------------- Part 9: ----------------------------");
                    Console.WriteLine("What ID do you want to check? (ID #56 got ties)");
                    int chosenId2 = Convert.ToInt32(Console.ReadLine());
                    _customerGenreRepository.GetAllById(chosenId2).ForEach(c => Console.WriteLine(c));
                    return true;
                default:
                    return true;
            }    
        }
    }
}
