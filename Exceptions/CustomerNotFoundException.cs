﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Exceptions
{
    /// <summary>
    /// Exception to be thrown when we can't find any customers. 
    /// </summary>
    internal class CustomerNotFoundException : Exception
    {
        public CustomerNotFoundException(string? message) : base(message)
        {

        }
    }
}
