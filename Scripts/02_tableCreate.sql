USE SuperheroesDB;
GO

CREATE TABLE Superhero(
Id Int IDENTITY(1,1) PRIMARY KEY,
Name Varchar(100),
Alias Varchar(100),
Origin Varchar(100)
);
GO

CREATE TABLE Assistant(
Id Int IDENTITY(1,1) PRIMARY KEY,
Name Varchar(100),
);

GO

CREATE TABLE Power(
Id Int IDENTITY(1,1) PRIMARY KEY,
Name Varchar(100),
Description Varchar(256),
);
GO