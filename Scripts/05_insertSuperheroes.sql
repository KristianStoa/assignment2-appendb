USE SuperheroesDB
GO

INSERT INTO Superhero (Name, Alias, Origin)
VALUES 
('Knut von Trut', 'Knutiboi', 'T-Town'),
('Espen Askebakken', 'Groundhog', 'L-Hammer'),
('Kristian Livingroom', 'LazyK', 'Tiger-City');
