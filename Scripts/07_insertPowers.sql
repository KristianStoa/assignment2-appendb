USE SuperheroesDB
GO

INSERT INTO Power(Name, Description)
VALUES
('Float a boat', 'Float a boat with your mind :O'),
('Dream viewer', 'View the dreams of others as clearly as watching TV'),
('Super luck', 'Beat the odds, have good fortune, and narrowly avoid misfortune'),
('Animal blender', 'Lead or live among animals, and be accepted as one of their own');

GO

INSERT INTO SuperheroPower(SuperheroId, PowerId)
VALUES
(1, 1),
(2, 1),
(2, 2),
(3, 4);