﻿using Assignment2_AppendixB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Repositories.Spender
{
    internal interface ICustomerSpenderRepository
    {
        List<CustomerSpender> GetAll();
    }
}
