﻿using Assignment2_AppendixB.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Repositories.Spender
{
    
    internal class CustomerSpenderRepositoryImpl : ICustomerSpenderRepository
    {
        private readonly string _connectionString;

        public CustomerSpenderRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }
        /// <summary>
        /// Method for task 8. 
        /// Gets all customers from database and how much they have spent on music. 
        /// Sorts them descending. 
        /// </summary>
        public List<CustomerSpender> GetAll()
        {
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = 
                "SELECT C.FirstName, C.LastName, Sum(I.Total) AS Total " +
                "FROM Customer AS C LEFT JOIN Invoice AS I " +
                "ON C.CustomerId = I.CustomerId " +
                "GROUP BY C.CustomerId, C.FirstName, C.LastName " +
                "ORDER BY Total DESC;";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader sqlDataReader = command.ExecuteReader();

            while (sqlDataReader.Read())
            {
                customerSpenders.Add(new CustomerSpender(
                    sqlDataReader.IsDBNull(0) ? "" : sqlDataReader.GetString(0),
                    sqlDataReader.IsDBNull(1) ? "" : sqlDataReader.GetString(1),
                    sqlDataReader.IsDBNull(2) ? 0 : sqlDataReader.GetDecimal(2)

                    )) ;
            }

            return customerSpenders;

        }
    }
}
