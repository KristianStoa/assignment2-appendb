﻿using Assignment2_AppendixB.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Repositories.Country
{
    internal class CustomerCountryRepositoryImpl : ICustomerCountryRepository
    {
        private readonly string _connectionString;

        public CustomerCountryRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }
        /// <summary>
        /// Method to select all countries and count the number of customers in them. 
        /// Returns country & number of customers. 
        /// </summary>
        public List<CustomerCountry> GetAll()
        {
            List<CustomerCountry> customerCountries = new List<CustomerCountry>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = 
                "SELECT Country, COUNT (*) AS Number " +
                "FROM Customer GROUP BY Country " +
                "ORDER BY Number DESC";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader sqlDataReader = command.ExecuteReader();

            while (sqlDataReader.Read())
            {
                customerCountries.Add(new CustomerCountry(
                    sqlDataReader.IsDBNull(0) ? "" : sqlDataReader.GetString(0),
                    sqlDataReader.GetInt32(1)
                    
                    ));
            }

            return customerCountries;
        }

    }
}
