﻿using System;
using Assignment2_AppendixB.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Repositories.Customers
{
    internal interface ICustomerRepository : ICrudRepository <Customer, int>
    {
    }
}
