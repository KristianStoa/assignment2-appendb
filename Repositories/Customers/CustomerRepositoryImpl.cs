﻿using System;
using Assignment2_AppendixB.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Assignment2_AppendixB.Exceptions;

namespace Assignment2_AppendixB.Repositories.Customers
{
    /// <summary>
    /// Methods for task 1 - 6. 
    /// </summary>
    internal class CustomerRepositoryImpl : ICustomerRepository
    {
        private readonly string _connectionString;

        public CustomerRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Task 5 
        /// Adds new customer to database
        /// </summary>
        public void Add(Customer obj)
        {
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = 
                "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@FirstName", obj.FirstName);
            command.Parameters.AddWithValue("@LastName", obj.LastName);
            command.Parameters.AddWithValue("@Country", obj.Country);
            command.Parameters.AddWithValue("@PostalCode", obj.PostalCode);
            command.Parameters.AddWithValue("@Phone", obj.Phone);
            command.Parameters.AddWithValue("@Email", obj.Email);
            command.ExecuteNonQuery();
        }
        /// <summary>
        /// Task 1
        /// Gets all customers from database and retrun them in a list.
        /// </summary>
        public List<Customer> GetAll()
        {
            List<Customer> customers = new List<Customer>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = 
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader sqlDataReader = command.ExecuteReader();
            
                while (sqlDataReader.Read())
                {
                    customers.Add(new Customer(
                        sqlDataReader.GetInt32(0),
                        sqlDataReader.GetString(1),
                        sqlDataReader.GetString(2),
                        sqlDataReader.IsDBNull(3) ? "" : sqlDataReader.GetString(3),
                        sqlDataReader.IsDBNull(4) ? "" : sqlDataReader.GetString(4),
                        sqlDataReader.IsDBNull(5) ? "" : sqlDataReader.GetString(5),
                        sqlDataReader.GetString(6)
                        ));
                }
            
            return customers;
        }
        /// <summary>
        /// Task 2
        /// Gets a customer by its ID and returns them as a Customer. Throws exception when 
        /// Id does not exist
        /// </summary>
        public Customer GetByID(int id)
        {
            Customer customer;
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = 
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer WHERE CustomerId = @Id";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader sqlDataReader = command.ExecuteReader();
            if (sqlDataReader.Read())
            {
                customer = new Customer(
                    sqlDataReader.GetInt32(0),
                    sqlDataReader.GetString(1),
                    sqlDataReader.GetString(2),
                    sqlDataReader.IsDBNull(3) ? "" : sqlDataReader.GetString(3),
                    sqlDataReader.IsDBNull(4) ? "" : sqlDataReader.GetString(4),
                    sqlDataReader.IsDBNull(5) ? "" : sqlDataReader.GetString(5),
                    sqlDataReader.GetString(6)

                    );
            }
            else
            {
                throw new CustomerNotFoundException("No Customer with that ID exists");
            }
            return customer;
        }
        /// <summary>
        /// Task 3
        /// Gets a customer from database by its name and returns it as a Customer. 
        /// Throws exception when name is not found. 
        /// </summary>
        public Customer GetByName(string Name)
        {
            Customer customer;
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = 
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer WHERE FirstName LIKE @Name;";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Name", Name);
            using SqlDataReader sqlDataReader = command.ExecuteReader();
            if (sqlDataReader.Read())
            {
                customer = new Customer(
                    sqlDataReader.GetInt32(0),
                    sqlDataReader.GetString(1),
                    sqlDataReader.GetString(2),
                    sqlDataReader.IsDBNull(3) ? "" : sqlDataReader.GetString(3),
                    sqlDataReader.IsDBNull(4) ? "" : sqlDataReader.GetString(4),
                    sqlDataReader.IsDBNull(5) ? "" : sqlDataReader.GetString(5),
                    sqlDataReader.GetString(6)

                    );
            }
            else
            {
                throw new CustomerNotFoundException("No Customer with that name.");
            }
            return customer;
        }
        /// <summary>
        /// Task 4
        /// Gets a page of customers and return them as a list of customers. 
        /// </summary>
        public List<Customer> GetAPage()
        { 
            List<Customer> customers = new List<Customer>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = 
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "ORDER BY Country " +
                "OFFSET 2 ROWS FETCH NEXT 5 ROWS ONLY";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader sqlDataReader = command.ExecuteReader();
            
                while (sqlDataReader.Read())
                {
                    customers.Add(new Customer(
                        sqlDataReader.GetInt32(0),
                        sqlDataReader.GetString(1),
                        sqlDataReader.GetString(2),
                        sqlDataReader.IsDBNull(3) ? "" : sqlDataReader.GetString(3),
                        sqlDataReader.IsDBNull(4) ? "" : sqlDataReader.GetString(4),
                        sqlDataReader.IsDBNull(5) ? "" : sqlDataReader.GetString(5),
                        sqlDataReader.GetString(6)
                        ));
                }

            return customers;
        }
        /// <summary>
        /// Task 6
        /// Method to update a customer in database.
        /// Can update FirstName, LastName, Phone, Country, PostalCode and Email. 
        /// </summary>
        public void Update(Customer obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = 
                "UPDATE Customer " +
                "SET FirstName = @FirstName, LastName = @LastName, Phone = @Phone, Country = @Country, PostalCode = @PostalCode, Email = @Email " +
                "WHERE CustomerId = @CustomerId ";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@CustomerId", obj.CustomerId);
            command.Parameters.AddWithValue("@FirstName", obj.FirstName);
            command.Parameters.AddWithValue("@LastName", obj.LastName);
            command.Parameters.AddWithValue("@Country", obj.Country);
            command.Parameters.AddWithValue("@PostalCode", obj.PostalCode);
            command.Parameters.AddWithValue("@Phone", obj.Phone);
            command.Parameters.AddWithValue("@Email", obj.Email);
            command.ExecuteNonQuery();
        }
    }
}
