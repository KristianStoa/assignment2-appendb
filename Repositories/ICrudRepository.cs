﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Repositories
{
    internal interface ICrudRepository<T, ID>
    {
        List<T> GetAll();

        T GetByID(ID id);

        T GetByName(string Name);

        List<T> GetAPage();

        void Add(T obj);

        void Update(T obj);
    }
}
