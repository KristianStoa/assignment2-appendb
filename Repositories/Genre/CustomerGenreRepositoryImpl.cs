﻿using Assignment2_AppendixB.Exceptions;
using Assignment2_AppendixB.Models;
using Assignment2_AppendixB.Repositories.Genre;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Repositories.Genre
{
    internal class CustomerGenreRepositoryImpl : ICustomerGenreRepository
    {
        private readonly string _connectionString;

        public CustomerGenreRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }
        /// <summary>
        /// Method for task 9. 
        /// Gets a customer by its id and returns its most popular genre(s) based on invoices associated to that customer. 
        /// In case of a tie, it shows both. 
        /// </summary>
        public List<CustomerGenre> GetAllById(int id)
        {
            List<CustomerGenre> customerGenre = new List<CustomerGenre>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = 
                "SELECT TOP 1 WITH TIES Count(G.Name) AS NumberOfGenre, C.CustomerId, C.FirstName, C.LastName, G.Name AS Genre " +
                "FROM Customer AS C " +
                "LEFT JOIN Invoice AS I ON C.CustomerId = I.CustomerId " +
                "LEFT JOIN InvoiceLine AS IL ON I.InvoiceId = IL.InvoiceId " +
                "LEFT JOIN Track AS T ON IL.TrackId = T.TrackId " +
                "LEFT JOIN Genre AS G ON T.GenreId = G.GenreId " +
                "WHERE C.CustomerId = @Id GROUP BY C.CustomerId, C.FirstName, C.LastName, G.Name " +
                "ORDER BY C.FirstName, C.LastName, NumberOfGenre DESC";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader sqlDataReader = command.ExecuteReader();
            while (sqlDataReader.Read())
            {
                customerGenre.Add(new CustomerGenre(
                    sqlDataReader.GetInt32(0),
                    sqlDataReader.GetInt32(1),
                    sqlDataReader.GetString(2),
                    sqlDataReader.GetString(3),
                    sqlDataReader.IsDBNull(4) ? "" : sqlDataReader.GetString(4)
                    ));
            }
            return customerGenre;
        }
    }
}
