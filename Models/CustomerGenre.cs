﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Models
{
    /// <summary>
    /// Model for CustomerGenre and its fields for this assignment.
    /// </summary>
    internal readonly record struct CustomerGenre(int NumberOfGenre, int CustomerId, string FirstName, string LastName, string Genre)
    {
    }
}
