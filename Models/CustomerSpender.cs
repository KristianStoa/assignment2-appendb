﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Models
{
    /// <summary>
    /// Model for CustomerSpender and its fields for this assignment. 
    /// </summary>
    internal readonly record struct CustomerSpender(string FirstName, string LastName, decimal Total)
    {
    }
}
