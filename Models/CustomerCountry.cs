﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Models
{
    /// <summary>
    /// Model for CustomerCountry and its fields used in the assignment
    /// </summary>
    internal readonly record struct CustomerCountry(string Country, int Number)
    {
    }
}
