﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2_AppendixB.Models
{
    /// <summary>
    /// Model for a customer. Database consists of more fields, but we are only using these for this assignment. 
    /// </summary>
    internal readonly record struct Customer (int CustomerId, string FirstName, string LastName, string Country, string PostalCode, string Phone, string Email)
    {
    }
}
