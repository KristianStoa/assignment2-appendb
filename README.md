## Name
Assignment 5 Appendix B

## Description
This project consist of SQL-calls to a personal SSMS server. Project is written in .NET 6.

## Installation
Clone this project. Make sure you have .NET 6 installed, and SSMS installed. Please alter the ConnectionString in Program.cs to your own ConnectionString. 

## Usage
This Project can be run with Run-command in your IDE, or by using:
```
dotnet build --no-restore
```
in the terminal.

To test the SQL-scripts open SSMS and select the files. Run them in numeric order (Eg. 01_xx first, then 02_xx and so on).

## Contributors
Espen Bakken

Kristian Stoa Mathisen

## License
Use as you wish in your own repository. Please do not override excisting repo. 


