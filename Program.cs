﻿using Assignment2_AppendixB.Repositories.Customers;
using Assignment2_AppendixB.Repositories.Country;
using Assignment2_AppendixB.Utils;
using Microsoft.Data.SqlClient;
using Assignment2_AppendixB.Repositories.Spender;
using Assignment2_AppendixB.Repositories.Genre;

namespace Assignment2_AppendixB
{
    internal class Program
    {

        static void Main(string[] args)
        {
            ICustomerRepository customerRepository = new CustomerRepositoryImpl(GetConnectionstring());
            ICustomerCountryRepository customerCountry = new CustomerCountryRepositoryImpl(GetConnectionstring());
            ICustomerSpenderRepository customerSpenderRepository = new CustomerSpenderRepositoryImpl(GetConnectionstring());
            ICustomerGenreRepository customerGenreRepository = new CustomerGenreRepositoryImpl(GetConnectionstring());
            DataAccessClient client = new DataAccessClient(customerRepository, customerCountry, customerSpenderRepository, customerGenreRepository);
            client.ExecuteCustomerDataAccess();
            bool isRunning = true;
            while (isRunning)
            {
                isRunning = client.ExecuteCustomerDataAccess();
            }

        }
        private static string GetConnectionstring()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "N-NO-01-01-0492\\SQLEXPRESS";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            builder.TrustServerCertificate = true;
            return builder.ConnectionString;
        }
    }
}